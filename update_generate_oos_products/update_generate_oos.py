#!/usr/bin/env python3
########################################################################################################################

from gspread.models import Cell
from datetime import datetime, timedelta
import os
import sys
import csv
import base64
import pandas as pd
import numpy as np
from pprint import pprint
from pymongo import MongoClient
from bson import ObjectId
import requests
import json
from pprint import pprint

from google_sheet_api import GoogleSheetApi

# mongo_ip = 'localhost'
mongo_ip = '34.225.140.74'

# grab_mongo_ip = 'localhost'
grab_mongo_ip = "34.234.68.234"


class Main(object):
    
    def __init__(self):
        self.oos_file_name = 'product_oos_csv_04sept2020_update.csv' #'product_oos_csv_03sept2020_update_new.csv' #'product_oos_csv_03sept2020_update.csv' # 'product_oos_csv_01sept2020_update_2.csv' #'product_oos_csv_26aug2020_update.csv' #'product_oos_csv_24aug2020_update.csv' #'oos_status_21aug.csv' # 'oos_status_20aug.csv'  #'oos_sheet_19aug2020.csv'

        ### Connect Videoproduct_commerece Database
        self.mongo_ip = mongo_ip
        self.mongo_client = MongoClient('mongodb://'+self.mongo_ip+':27017/')
        self.videocommerce_demo = self.mongo_client['videocommerce_demo_6']
        self.videoproducts = self.videocommerce_demo['videoproducts']

        ### Connect BHG Database
        self.grab_mongo_ip = grab_mongo_ip
        self.mongo_client2 = MongoClient('mongodb://'+self.grab_mongo_ip+':27017/')
        self.accio_bhg_db = self.mongo_client2['accio_bhg']
        self.product = self.accio_bhg_db['product']

        self.google_sheet_file = 'Zee5 TWM - OOS '                  #'videoproducts_oos_links'
        self.gsheet_api = False

    
    def start(self):
        self.update_productvideo_product_link()
        print ("Process done.")


    def update_productvideo_product_link(self):
        print ("Update OOS Produc link inprocess.....", self.oos_file_name)
        # Update PostId Data
        # postId_list = ["a0317b82c2166ad40e0b908b6555c0c0", "789bd9bbfc84606375256475f0596139"]

        #20aug
        # postId_list = ['a0317b82c2166ad40e0b908b6555c0c0', '789bd9bbfc84606375256475f0596139']
        #21aug
        # postId_list = ['8023c1a887a18c0fd8832928ad6a29c5', '8a9a0112165e244295f98ec5a1667fd9']
        #26aug
        postId_list = ["8023c1a887a18c0fd8832928ad6a29c5", "8a9a0112165e244295f98ec5a1667fd9", "a0317b82c2166ad40e0b908b6555c0c0", "789bd9bbfc84606375256475f0596139"]


        source = "amazon"
        # api_url = 'https://product.accio.ai/scrap_detail/'
        api_url_local = 'http://127.0.0.1:8000/scrap_detail/'

        ### Read CSV File and update the Product Price and create new product using new url for the OOS products. Replace with existing Product data
        csv_file_data = pd.read_csv(self.oos_file_name)               # Read Local File
        product_df = pd.DataFrame(data=csv_file_data).replace(to_replace=np.nan, value=False)                 # DataFrame
        pprint (product_df)
       
        videoproducts_res  = list(self.videoproducts.find({'postId': {'$in': postId_list}, 'products': {'$exists': True}}))
        print (f"Videoproduct count: {len(videoproducts_res)}")
        
        total_matched_product_list = []
        total_matched_product_count = 0
        count = 1
        mcount = 1
        updated_record_count = 1
        product_not_matched_count = 1
        product_not_matched_list = []
        for vres_data in videoproducts_res:
            print ("Count: ", count)
            if not vres_data.get('products'):
                continue

            products_data_list = vres_data["products"]
            product_data_updated = False
            products_data_new_list = []
            product_no_count = len(vres_data["products"])

            for prod_data in vres_data["products"]:
                if prod_data['source'] == source:
                    # print ('Amzaone==============================')
                    product_matched_df = product_df.loc[product_df['_id'] == str(prod_data["_id"])]
                    print ("product_matched_df Count: ", len(product_matched_df.index))

                    if len(product_matched_df.index) > 0:
                        total_matched_product_count += 1
                        # mcount += 1
                        try:
                            for index, pd_data in product_matched_df.iterrows():
                                print ("pd_data: ", type(pd_data['link_updated']))
                                # if isinstance(pd_data['inStock_updated'], float) and pd_data['inStock_updated'] == 0.0:
                                if isinstance(pd_data['link_updated'], str) and pd_data['link_updated']:
                                    # Grab New given link....Created Product Object data update here...delete existing from here
                                    print ("Grab link_updated data: ")
                                    if pd_data['link_updated']:
                                        prod_response_json = requests.post(url=api_url_local, data={'start_url': pd_data['link_updated']}, timeout=200)
                                        # prod_response_json = requests.post(url=api_url, data=data, timeout=200)
                                        # print ("json data=========================>>>", json.loads(prod_response_json.text))
                                        product_json_data = json.loads(prod_response_json.text)["result"]
                                        if product_json_data:
                                            # print ("Produc Json Data====================")
                                            pprint(product_json_data)
                                            if product_json_data.get("_id"):
                                                product_res_data = list(self.product.find({"_id": ObjectId(product_json_data["_id"])}))
                                                # print ("product_res_data=====================", product_res_data)
                                                products_data_new_list.append(product_res_data[0])
                                                product_data_updated = True
                                                mcount += 1
                                            else:
                                                products_data_new_list.append(prod_data)
                                            # products_data_new_list.append(product_json_data)
                                            # product_data_updated = True
                                        else:
                                            products_data_new_list.append(prod_data)
                                    else:
                                        products_data_new_list.append(prod_data)
                                else:
                                    # Price Update  for the existing
                                    print ("sellingPrice_updated: ", pd_data['sellingPrice_updated'], prod_data.get('sellingPrice'))
                                    if pd_data['sellingPrice_updated'] and prod_data.get('sellingPrice') != pd_data['sellingPrice_updated']:
                                        print ("Update SellingPrice...........")
                                        new_prod_data = prod_data
                                        new_prod_data['sellingPrice'] = pd_data['sellingPrice_updated']
                                        products_data_new_list.append(new_prod_data)
                                        product_data_updated = True
                                        mcount += 1
                                    else:
                                        products_data_new_list.append(prod_data)
                                break
                        except Exception as e:
                            print ("Error: ", e)
                            products_data_new_list.append(prod_data)
                    else:
                        products_data_new_list.append(prod_data)
                else:
                    products_data_new_list.append(prod_data)

            
            print ("Product ShouldUpdate-->Product Actual Count--->Update Count=========>>>>", product_data_updated, product_no_count, len(products_data_new_list))


            if len(products_data_new_list) != product_no_count:
                print ("Product CountNot Matched=-===========================")
                product_not_matched_count += 1
                product_not_matched_list.append(vres_data["_id"])
            # pprint(products_data_new_list)

            if product_data_updated and len(products_data_new_list) > 0:
                print ("updated Product list data: ")
                self.videoproducts.update({"_id": vres_data["_id"]},
                                          {
                                            "$set": 
                                                {
                                                    "products": products_data_new_list
                                                }
                                          }
                                        )
                updated_record_count += 1
            count += 1
        print ("total_matched_product_count: ", total_matched_product_count, count, updated_record_count, product_not_matched_count)
        # total_matched_product_count:  4755 34338 4390
        print ("product_not_matched_list===>", len(product_not_matched_list), product_not_matched_list)


if __name__ == "__main__":
    main_obj = Main()
    main_obj.start()
