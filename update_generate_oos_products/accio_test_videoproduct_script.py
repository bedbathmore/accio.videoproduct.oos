#!/usr/bin/env python3
########################################################################################################################
# author : Zubair
# task : This script will generate csv file for articles with https://grabv1.accio.ai/curate/article/ObjectId()
########################################################################################################################

import os
import sys
from pymongo import MongoClient
import csv
from bson.objectid import ObjectId
from datetime import datetime
import threading
import socket
import dateutil.parser
import requests
import urllib.request
from pprint import pprint

hostname = socket.gethostname()


bhg_grab_mongo_ip = '34.234.68.234'
video_mongo_ip = '34.225.140.74'


class VideoProductProcess:

	def __init__(self):
		self.mongo_ip = bhg_grab_mongo_ip
		self.mongo_client = MongoClient('mongodb://'+self.mongo_ip+':27017/')
		self.accio_bhg = self.mongo_client['accio_bhg']
		self.article = self.accio_bhg['article']
		self.product = self.accio_bhg['product']

		self.vmongo_ip = video_mongo_ip
		self.video_mongo_client = MongoClient('mongodb://'+self.vmongo_ip+':27017/')
		self.videocommerce = self.video_mongo_client['videocommerce_demo_6']

		self.videoproducts = self.videocommerce['videoproducts']

		self.wfile_name = "video_products_details.csv"
		self.product_csv_file = "product_link_oos_csv_file_15sept2020_new.csv"
		
		# ssh -i "accio_production.pem" ubuntu@ec2-34-225-140-74.compute-1.amazonaws.com


	def start(self):
		self.video_process_product_details()
		print ("Process done......")


	def product_links_generate_csv_file(self, products_link_dict):
		print ("prouct links----------------------->>>")
		with open(self.wfile_name, 'w') as csvfile:
			fieldnames = ['postId', '_id', 'productId', 'source',  'link', 'sellingPrice', 'inStock', 'imageUrl']
			writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
			writer.writeheader()
			count = 1
			for postId, product_values in products_link_dict.items():
				product_data_res = list(self.product.find({'_id': {'$in': product_values}}))
				print ("product_data_res count : ", len(product_data_res))
				for product in product_data_res:
					product_vals = {'postId': postId,
									'_id': product.get('_id'), 
									'productId': product.get('productId'), 
									'source': product.get('source'),  
									'link': product.get('link'), 
									'sellingPrice': product.get('sellingPrice'), 
									'inStock': product.get('inStock'),
									'imageUrl': product.get('imgUrl')
					}
					writer.writerow(product_vals)


	def video_product_links_generate_csv_file(self, video_product_data_dict):
		print ("prouct links----------------------->>>")
		with open(self.product_csv_file, 'w') as csvfile:
			fieldnames = ['postId', '_id', 'productId', 'source',  'link', 'sellingPrice', 'inStock', 'imageUrl']
			writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
			writer.writeheader()
			count = 1
			for postId, product_values in video_product_data_dict.items():
				for vproduct in product_values:
					
					product_vals = {'postId': postId,
									'_id': vproduct.get('_id'), 
									'productId': vproduct.get('productId'), 
									'source': vproduct.get('source'),  
									'link': vproduct.get('link'), 
									'sellingPrice': vproduct.get('sellingPrice'), 
									'inStock': vproduct.get('inStock'),
									'imageUrl': vproduct.get('imgUrl')
					}
					writer.writerow(product_vals)


	def video_process_product_details(self):
		print ("video process details---------------------------", self.product_csv_file)
		# post_list_id = ['a0317b82c2166ad40e0b908b6555c0c0', '789bd9bbfc84606375256475f0596139']

		# New given by Prisilla episod
		# post_list_id = ["8023c1a887a18c0fd8832928ad6a29c5", "8a9a0112165e244295f98ec5a1667fd9"]

		post_list_id = ['a0317b82c2166ad40e0b908b6555c0c0', '789bd9bbfc84606375256475f0596139', "8023c1a887a18c0fd8832928ad6a29c5", "8a9a0112165e244295f98ec5a1667fd9"]

		# product_list_ids = []
		products_link_dict = {}
		video_product_data_dict = {}
		video_product_data_main_list = []
		for postid in post_list_id:
			print ("postid---------------------", postid)
			videoproducts_res = self.videoproducts.find({'postId': postid})
			print ("videoproducts_res============================", videoproducts_res.count())
			product_data_list = []
			video_product_data_list = []
			for v_product in videoproducts_res:
				if v_product.get('products'):
					for prod in v_product['products']:
						# if prod["_id"] not in product_data_list:
						product_data_list.append(ObjectId(prod["_id"]))

						if prod not in video_product_data_list:
							# if prod not in video_product_data_main_list:
							video_product_data_list.append(prod)


			print ("Total Product Counts: ", len(product_data_list), len(list(set(product_data_list))))
			print ("Total video Product Count: ", len(video_product_data_list))
			products_link_dict[postid] = list(set(product_data_list))
			video_product_data_dict[postid] = video_product_data_list

		# self.product_links_generate_csv_file(products_link_dict)
		self.video_product_links_generate_csv_file(video_product_data_dict)


def main():
	article = VideoProductProcess()
	article.start()
	print ("Process done.")


if __name__ == "__main__":
	main()