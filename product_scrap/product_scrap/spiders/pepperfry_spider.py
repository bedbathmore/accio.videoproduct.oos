import scrapy
from datetime import datetime as dt
from scrapy.http import FormRequest
from scrapy.http.request import Request

class PepperfrySpider(scrapy.Spider):
    name = 'pepperfry'
    base_url = 'https://www.pepperfry.com'
    hdr = {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0', 
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' ,
            'Accept-Language': 'en-US,en;q=0.5' ,
            'Referer': 'https://www.google.com/',
            'Upgrade-Insecure-Requests': '1' ,
            'Connection': 'keep-alive' 
        }

    def start_requests(self):
        return [
            FormRequest("https://www.pepperfry.com",headers=self.hdr,callback=self.parse,dont_filter = True),
            # FormRequest("https://www.pepperfry.com/cordoba-two-seater-sofa-in-charcoal-grey-colour-by-casacraft-1599727.html?type=clip&pos=33:1&catname=Sofas&total_result=1572&variation_id=200682",headers=self.hdr,callback=self.parse_product_detail,dont_filter = True),
            
        ]

    def parse(self, response):
        for i in response.xpath('//div[@id="megamenu"]//a[@class="hvr-col-listitem-link"]/@href').extract():
            url = i+"&p=1"
            page = 1
            yield Request(url,headers=self.hdr, callback=self.parse_product_list,meta={'page':page},dont_filter = True)
            # break

    def parse_product_list(self, response):
        # print(response.url)

        for i in response.xpath('//div[@id="productView"]/div/div/div[contains(@class,"card-img-wrp")]/a/@href').extract():
            # print(i)
            yield Request(i,headers=self.hdr, callback=self.parse_product_detail,dont_filter = True)


        if response.xpath('//div[@id="productView"]').extract():
            page = int(response.meta['page']) + 1
            n_url = response.url.split("p=")[0]+"p="+str(page)
            yield Request(n_url,headers=self.hdr, callback=self.parse_product_list,meta={'page':page},dont_filter = True)


    def parse_product_detail(self,response):
        # print(response.url)
        product = {}


        product['productId'] = response.url.split('.html')[0].split("-")[-1]
        product['sellingPrice'] = response.xpath('//meta[@itemprop="price"]/@content').extract_first()
        product['maxSellingPrice'] = product['sellingPrice']
        product['title'] = response.xpath('//h1[@itemprop="name"]/text()').extract_first().strip()
        imgurls = response.xpath('//img[@itemprop="image"]/@data-bigimg').extract()

        product['allimgUrls'] = imgurls
        product['imgUrl'] = product['allimgUrls'][0]

        product['description'] = ''.join(response.xpath('//meta[@property="og:description"]/@content').extract()).strip() 
        product['website'] = self.base_url
        product['updated'] = dt.now()
        product['brand'] = response.xpath('//input[@id="brand_name"]/@value').extract_first()
        product['link'] = response.url
        product['publisherID'] = ''
        product['affiliateNetwork'] = ''
   
        product['productType'] = 'home' 
        product['method'] = 'product_scrap'
        product['currency'] = 'INR'

        product['source'] = self.name
        product['country'] = 'IN' 
            
        product['inStock'] = True
        product['topSeller'] = False
        product['options'] = {}

        categories = response.xpath('//ul[@class="bredcrumb"]/span/li/a/span/text()').extract()
        # print(categories)
        try:
            product['mainCategory'] = [categories[-3]] 
        except:
            product['mainCategory'] = []

        try:
            product['midCategory'] = [categories[-2]] 
        except:
            product['midCategory'] = []

        try:
            product['category'] = [categories[-1]] 
        except:
            product['category'] = []


        
       
        # print(product)
        yield product
    
    def closed(self, reason):
        print("----------closed (new)-----------\n", reason)