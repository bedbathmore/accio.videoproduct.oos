# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo
from datetime import datetime as dt
from copy import deepcopy

class ProductScrapPipeline(object):
	def __init__(self, mongo_uri, mongo_db):
		self.mongo_uri = mongo_uri
		self.mongo_db = mongo_db
		self.created = 0
		self.updated = 0
		self.publisherId = None
		print('mongo details:', mongo_uri, mongo_db)
	
	@classmethod
	def from_crawler(cls, crawler):
		## pull in information from settings.py
		return cls(
			mongo_uri=crawler.settings.get('MONGO_URI'),
			mongo_db=crawler.settings.get('MONGO_DATABASE')
		)

	def close_spider(self, spider):
			print('\n\nCreated:',self.created, '\tUpdated:',self.updated, '\n')

	def process_item(self, item, spider):
		client = pymongo.MongoClient(self.mongo_uri)
		db = client[self.mongo_db]
	
		product = db['product'].update({'source':item['source'],'productId':item['productId']},
			{
				'$set':item,
				'$setOnInsert': {'created':dt.now()}
			},upsert=True)
		
		client.close()
		return item